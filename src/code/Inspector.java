package code;

import java.lang.reflect.*;

/**
 * CPSC 501
 * Inspector starter class
 *
 * @author Jonathan Hudson
 */
public class Inspector {


	public void inspect(Object obj, boolean recursive) {
        Class c = obj.getClass();
        inspectClass(c, obj, recursive, 0);
        
    }

    private void inspectClass(Class c, Object obj, boolean recursive, int depth) {
    	
		//Get the amount of tabs we need 
		String tabsDepth = "\t".repeat(depth);
		
		//----------------------------ClassPart----------------------------------//

		System.out.println("\n" + tabsDepth + "CLASS");
		
		// Get class name
		String className = c.getSimpleName();
		
		// Print class name
		System.out.println(tabsDepth + "Class: " + className);
		
		
		//----------------------------SuperClassPart----------------------------------//
	
		System.out.println("\n" + tabsDepth + "SUPERCLASS");
		
		//Get superclass c
		Class superClass = c.getSuperclass();
		
		//No superclass
		if (superClass == null) {
			System.out.println(tabsDepth + "SuperClass: NONE");
		}
		
		else {
			//Get superclass name
			String superclassName = superClass.getName();
			
			//Print superclass name
			System.out.println(tabsDepth + "Super Class: " + superclassName);
			
			//Recursively explore superclass
			inspectClass(superClass, obj, recursive, depth+1);
		}
		
		//----------------------------InterfacePart----------------------------------//
		System.out.println("\n" + tabsDepth + "INTERFACES");
		
		//Get list of interfaces
		Class[] interfaceObj = c.getInterfaces();
		
		//No interfaces
		if (interfaceObj.length == 0) {
			System.out.println(tabsDepth + "Interfaces-> NONE");
		}
		else {
			
			//Loop through all interfaces
			for(Class i: interfaceObj) {
				
				//Get name
				String interfaceName = i.getName();
				
				System.out.println(tabsDepth + interfaceName);
				
				//Recursively explore interfaces
				inspectClass(i, obj, recursive, depth+1);

			}
		}
		//--------------------------ConstructorsPart---------------------------------//
		
		System.out.println("\n" + tabsDepth + "CONSTRUCTORS");
		
		//Get list of constructors
		Constructor[] constructorsObj = c.getDeclaredConstructors();
		
		//No constructors
		if (constructorsObj.length == 0) {
			System.out.println(tabsDepth + "Constructors-> NONE");
		}
		else {
			
			//Loop through all constructors
			for(Constructor i: constructorsObj) {
				
				Class[] parametersObj = i.getParameterTypes();
				
				Class[] exceptionObj = i.getExceptionTypes();
				//Get name
				String constructorName = i.getName();
				
				System.out.println(tabsDepth + constructorName);
				
				//--------------------------ParametersPart(Constructors)-----------------------//
				if(parametersObj.length == 0) {
					System.out.println(tabsDepth+ " " + "Parameter: NONE");
				}
				else {
					//Loop through all parameters
					for(Class j: parametersObj) {
						
						String parameterName = j.getName();
						
						System.out.println(tabsDepth+ " " + "Parameter: " + parameterName);
					}
				}
				
				//--------------------------ExceptionsPart(Constructors)-----------------------//
				if(exceptionObj.length == 0) {
					System.out.println(tabsDepth+ " " + "Exceptions: NONE");
				}
				else {
					//Loop through all exceptions
					for(Class j: exceptionObj) {
						
						String exceptionName = j.getName();
						
						System.out.println(tabsDepth+ " " + "Exception: " + exceptionName);
					}
				}
				
				
				//--------------------------ModifiersPart(Constructors)-----------------------//
				int modifierObj = i.getModifiers();
				
				if(modifierObj == 0) {
					System.out.println(tabsDepth+ " " + "Modifiers: NONE");
				}
				else {
					String mod = tabsDepth+ " " + "Modifiers: " + Modifier.toString(modifierObj);
					System.out.println(mod);
					
				}
			}
		}
		
		//----------------------------MethodsPart----------------------------------------//
		
		System.out.println("\n" + tabsDepth + "METHODS");
		
		Method[] methods = c.getMethods();
		
		if(methods.length == 0) {
			System.out.println(tabsDepth + "Methods: NONE");
		}
		else {
			//Loop through all methods and get name, exceptions, parameter types, return type and modifiers
			for(Method i: methods) {
				Class[] exceptionsObj = i.getExceptionTypes();
				Class[] parametersObj = i.getParameterTypes();
				Class returnObj = i.getReturnType();
				int modifierObj = i.getModifiers();
				String methodName = i.getName();
				System.out.println(tabsDepth + methodName);
				
				//-------------------------ExceptionsPart(Methods)---------------------------//
				if(exceptionsObj.length == 0) {
					System.out.println(tabsDepth + " " + "Exceptions: NONE");
				}
				else {
					//Loop through all exceptions
					for(Class j: exceptionsObj) {
						
						String exceptionName = j.getName();
						
						System.out.println(tabsDepth + " " + "Exception: " + exceptionName);
					}
				}
				//-------------------------ParametersPart(Methods)---------------------------//
				if(parametersObj.length == 0) {
					System.out.println(tabsDepth + " " + "Parameter: NONE");
				}
				else {
					//Loop through all parameters
					for(Class j: parametersObj) {
						
						String parameterName = j.getName();
						
						System.out.println(tabsDepth + " " + "Parameter: " + parameterName);
					}
				}
				//-------------------------ReturnPart(Methods)-------------------------------//
				if(returnObj == null) {
					System.out.println(tabsDepth + " " + "Return Type: NONE");
				}
				else {
					String returnObjName = returnObj.getName();
					System.out.println(tabsDepth + " " + "Return Type: " + returnObjName);
				}
				//-------------------------ModifiersPart(Methods)----------------------------//	
				if(modifierObj == 0) {
					System.out.println(tabsDepth + "Modifiers: NONE");
				}
				else {
					String mod = tabsDepth+ " " + "Modifiers: " + Modifier.toString(modifierObj);
					System.out.println(mod);
					
				}
			}
		}
		//-------------------------FieldsPart---------------------------------//
		System.out.println("\n" + tabsDepth + "FIELDS");
		Field[] fields = c.getFields();
		if(fields.length == 0) {
			System.out.println(tabsDepth + "Fields: NONE");
		}
		else {
			
			//Loop through all fields declared
			for(Field i: fields) {
				
				//Field method to suppress checks for Java language access control when used.
				i.setAccessible(true);
				
				String fieldName = i.getName();
				int modifierObj = i.getModifiers();
				Class fieldType = i.getType();
				
				System.out.println(tabsDepth + fieldName);
				//-------------------------TypePart(Methods)---------------------------------//
				if (fieldType == null) {
					System.out.println(tabsDepth + "Types: NONE");
				}
				else {
					String typeName = fieldType.getName();
					System.out.println(tabsDepth + " " + "Type: " + typeName);
				}
				//-------------------------ModifiersPart(Methods)----------------------------//
				if(modifierObj == 0) {
					System.out.println(tabsDepth + "Modifiers: NONE");
				}
				else {
					String mod = tabsDepth + " " + "Modifiers: " + Modifier.toString(modifierObj);
					System.out.println(mod);
				}
				
				//Not too sure if code after this point is implemented correctly.
				if(fieldType.isPrimitive()) {
					try {
						Object fieldObj = i.get(obj);
						System.out.println(tabsDepth + "Value: " + fieldObj);
					}
					catch(IllegalAccessException e) {
						e.printStackTrace();
					}
					catch(IllegalArgumentException e){
						e.printStackTrace();
					}
				}
				else {
					if(fieldType.isArray()) {
						try {
							Object fieldObj = i.get(obj);
							int arrayLength = Array.getLength(fieldType);
							if(arrayLength == 0)
							{
								System.out.println(tabsDepth + "Array: NONE");
							}
							else {
								Class arrayComponentType = fieldType.getComponentType();
								System.out.println(tabsDepth + "Component Type: " + arrayComponentType);
								System.out.println(tabsDepth + "Length: " + arrayLength);
								for(int j = 0; j < arrayLength; j++) {
									System.out.println(tabsDepth + "Value: " + j);
									//Recursively explore interfaces
									inspectClass(fieldType, obj, recursive, depth+1);
								}
								
								
							}
						}
						catch(IllegalAccessException e) {
							
						}
						catch(IllegalArgumentException e) {
							e.printStackTrace();
						}
					}
					else {
						System.out.println("We never enter the above conditionals :(");
					}
				}
			}
		}
    }
}
