# CPSC501A2

I successfully get the name of the declaring class 

I successfully get the name of the immediate super class and recursively explore the superclass.

I successfully get the name of each interface and recursively explore the interfaces.

I successfully get the constructors the class declares and find the name, exceptions, parameter types and modifiers.

I successfully get the methods the class declares and find the name, exceptions, parameter types, return type and modifiers.

I successfully get the fields the class declares and find the name, type and modifiers. However, I wasn't able to fully get the code for the other portion of the assignment.

---------------------------------------------------------------------------------------------------------------------------------------------------

**Refactor**
1. What code in which files was altered?
In Inspector.java, multiple variable names were changed in inspectClass().

parameterObj -> parameterTypes

exceptionObj -> exceptionTypes

mod -> displayMod

2. What needed to be improved?
The bad code smell detected was Worrisome Comments. The comments made for some code was better than the name I gave to them.

3. What refactoring was applied?
The Rename Method was used to refactor my code.

4. What code in which files was the result of the refactoring?
Lines 100, 102, 109, 114, 123, 128, 144, and 145 were altered.

5. The variable names better reflect their purpose and give whoever is looking at my code as well as myself a better understanding of what my code is supposed to do.

6. Does the result of the refactoring suggest or enable further refactorings?
No further refactorings needed.

7. SHA?
SHA: 7a567947b03a93dee40dc635c562a9b4448e5b70

I also changed how the output was displayed to be more readable. But, I am not sure if this counts as a refactor.
