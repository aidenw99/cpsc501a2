======================================================
Filename: script8.txt
Running Test: Test String
Recursive: true

CLASS
Class: String

SUPERCLASS
Super Class: java.lang.Object

	CLASS
	Class: Object

	SUPERCLASS
	SuperClass: NONE

	INTERFACES
	Interfaces-> NONE

	CONSTRUCTORS
	java.lang.Object
	 Parameter: NONE
	 Exceptions: NONE
	 Modifiers: public

	METHODS
	wait
	 Exception: java.lang.InterruptedException
	 Parameter: long
	 Parameter: int
	 Return Type: void
	 Modifiers: public final
	wait
	 Exception: java.lang.InterruptedException
	 Parameter: NONE
	 Return Type: void
	 Modifiers: public final
	wait
	 Exception: java.lang.InterruptedException
	 Parameter: long
	 Return Type: void
	 Modifiers: public final native
	equals
	 Exceptions: NONE
	 Parameter: java.lang.Object
	 Return Type: boolean
	 Modifiers: public
	toString
	 Exceptions: NONE
	 Parameter: NONE
	 Return Type: java.lang.String
	 Modifiers: public
	hashCode
	 Exceptions: NONE
	 Parameter: NONE
	 Return Type: int
	 Modifiers: public native
	getClass
	 Exceptions: NONE
	 Parameter: NONE
	 Return Type: java.lang.Class
	 Modifiers: public final native
	notify
	 Exceptions: NONE
	 Parameter: NONE
	 Return Type: void
	 Modifiers: public final native
	notifyAll
	 Exceptions: NONE
	 Parameter: NONE
	 Return Type: void
	 Modifiers: public final native

	FIELDS
	Fields: NONE

INTERFACES
java.io.Serializable

	CLASS
	Class: Serializable

	SUPERCLASS
	SuperClass: NONE

	INTERFACES
	Interfaces-> NONE

	CONSTRUCTORS
	Constructors-> NONE

	METHODS
	Methods: NONE

	FIELDS
	Fields: NONE
java.lang.Comparable

	CLASS
	Class: Comparable

	SUPERCLASS
	SuperClass: NONE

	INTERFACES
	Interfaces-> NONE

	CONSTRUCTORS
	Constructors-> NONE

	METHODS
	compareTo
	 Exceptions: NONE
	 Parameter: java.lang.Object
	 Return Type: int
	 Modifiers: public abstract

	FIELDS
	Fields: NONE
java.lang.CharSequence

	CLASS
	Class: CharSequence

	SUPERCLASS
	SuperClass: NONE

	INTERFACES
	Interfaces-> NONE

	CONSTRUCTORS
	Constructors-> NONE

	METHODS
	length
	 Exceptions: NONE
	 Parameter: NONE
	 Return Type: int
	 Modifiers: public abstract
	toString
	 Exceptions: NONE
	 Parameter: NONE
	 Return Type: java.lang.String
	 Modifiers: public abstract
	charAt
	 Exceptions: NONE
	 Parameter: int
	 Return Type: char
	 Modifiers: public abstract
	compare
	 Exceptions: NONE
	 Parameter: java.lang.CharSequence
	 Parameter: java.lang.CharSequence
	 Return Type: int
	 Modifiers: public static
	codePoints
	 Exceptions: NONE
	 Parameter: NONE
	 Return Type: java.util.stream.IntStream
	 Modifiers: public
	subSequence
	 Exceptions: NONE
	 Parameter: int
	 Parameter: int
	 Return Type: java.lang.CharSequence
	 Modifiers: public abstract
	chars
	 Exceptions: NONE
	 Parameter: NONE
	 Return Type: java.util.stream.IntStream
	 Modifiers: public

	FIELDS
	Fields: NONE
java.lang.constant.Constable

	CLASS
	Class: Constable

	SUPERCLASS
	SuperClass: NONE

	INTERFACES
	Interfaces-> NONE

	CONSTRUCTORS
	Constructors-> NONE

	METHODS
	describeConstable
	 Exceptions: NONE
	 Parameter: NONE
	 Return Type: java.util.Optional
	 Modifiers: public abstract

	FIELDS
	Fields: NONE
java.lang.constant.ConstantDesc

	CLASS
	Class: ConstantDesc

	SUPERCLASS
	SuperClass: NONE

	INTERFACES
	Interfaces-> NONE

	CONSTRUCTORS
	Constructors-> NONE

	METHODS
	resolveConstantDesc
	 Exception: java.lang.ReflectiveOperationException
	 Parameter: java.lang.invoke.MethodHandles$Lookup
	 Return Type: java.lang.Object
	 Modifiers: public abstract

	FIELDS
	Fields: NONE

CONSTRUCTORS
java.lang.String
 Parameter: [B
 Exceptions: NONE
 Modifiers: public
java.lang.String
 Parameter: [B
 Parameter: int
 Parameter: int
 Exceptions: NONE
 Modifiers: public
java.lang.String
 Parameter: [B
 Parameter: java.nio.charset.Charset
 Exceptions: NONE
 Modifiers: public
java.lang.String
 Parameter: [B
 Parameter: java.lang.String
 Exception: java.io.UnsupportedEncodingException
 Modifiers: public
java.lang.String
 Parameter: [B
 Parameter: int
 Parameter: int
 Parameter: java.nio.charset.Charset
 Exceptions: NONE
 Modifiers: public
java.lang.String
 Parameter: [C
 Parameter: int
 Parameter: int
 Parameter: java.lang.Void
 Exceptions: NONE
 Modifiers: NONE
java.lang.String
 Parameter: java.lang.AbstractStringBuilder
 Parameter: java.lang.Void
 Exceptions: NONE
 Modifiers: NONE
java.lang.String
 Parameter: java.lang.StringBuilder
 Exceptions: NONE
 Modifiers: public
java.lang.String
 Parameter: java.lang.StringBuffer
 Exceptions: NONE
 Modifiers: public
java.lang.String
 Parameter: [B
 Parameter: byte
 Exceptions: NONE
 Modifiers: NONE
java.lang.String
 Parameter: [C
 Parameter: int
 Parameter: int
 Exceptions: NONE
 Modifiers: public
java.lang.String
 Parameter: [C
 Exceptions: NONE
 Modifiers: public
java.lang.String
 Parameter: java.lang.String
 Exceptions: NONE
 Modifiers: public
java.lang.String
 Parameter: NONE
 Exceptions: NONE
 Modifiers: public
java.lang.String
 Parameter: [B
 Parameter: int
 Parameter: int
 Parameter: java.lang.String
 Exception: java.io.UnsupportedEncodingException
 Modifiers: public
java.lang.String
 Parameter: [B
 Parameter: int
 Exceptions: NONE
 Modifiers: public
java.lang.String
 Parameter: [B
 Parameter: int
 Parameter: int
 Parameter: int
 Exceptions: NONE
 Modifiers: public
java.lang.String
 Parameter: [I
 Parameter: int
 Parameter: int
 Exceptions: NONE
 Modifiers: public

METHODS
equals
 Exceptions: NONE
 Parameter: java.lang.Object
 Return Type: boolean
 Modifiers: public
length
 Exceptions: NONE
 Parameter: NONE
 Return Type: int
 Modifiers: public
toString
 Exceptions: NONE
 Parameter: NONE
 Return Type: java.lang.String
 Modifiers: public
hashCode
 Exceptions: NONE
 Parameter: NONE
 Return Type: int
 Modifiers: public
getChars
 Exceptions: NONE
 Parameter: int
 Parameter: int
 Parameter: [C
 Parameter: int
 Return Type: void
 Modifiers: public
compareTo
 Exceptions: NONE
 Parameter: java.lang.Object
 Return Type: int
 Modifiers: public volatile
compareTo
 Exceptions: NONE
 Parameter: java.lang.String
 Return Type: int
 Modifiers: public
indexOf
 Exceptions: NONE
 Parameter: int
 Parameter: int
 Return Type: int
 Modifiers: public
indexOf
 Exceptions: NONE
 Parameter: java.lang.String
 Return Type: int
 Modifiers: public
indexOf
 Exceptions: NONE
 Parameter: java.lang.String
 Parameter: int
 Return Type: int
 Modifiers: public
indexOf
 Exceptions: NONE
 Parameter: int
 Return Type: int
 Modifiers: public
valueOf
 Exceptions: NONE
 Parameter: [C
 Return Type: java.lang.String
 Modifiers: public static
valueOf
 Exceptions: NONE
 Parameter: java.lang.Object
 Return Type: java.lang.String
 Modifiers: public static
valueOf
 Exceptions: NONE
 Parameter: long
 Return Type: java.lang.String
 Modifiers: public static
valueOf
 Exceptions: NONE
 Parameter: [C
 Parameter: int
 Parameter: int
 Return Type: java.lang.String
 Modifiers: public static
valueOf
 Exceptions: NONE
 Parameter: int
 Return Type: java.lang.String
 Modifiers: public static
valueOf
 Exceptions: NONE
 Parameter: char
 Return Type: java.lang.String
 Modifiers: public static
valueOf
 Exceptions: NONE
 Parameter: double
 Return Type: java.lang.String
 Modifiers: public static
valueOf
 Exceptions: NONE
 Parameter: float
 Return Type: java.lang.String
 Modifiers: public static
valueOf
 Exceptions: NONE
 Parameter: boolean
 Return Type: java.lang.String
 Modifiers: public static
charAt
 Exceptions: NONE
 Parameter: int
 Return Type: char
 Modifiers: public
codePointAt
 Exceptions: NONE
 Parameter: int
 Return Type: int
 Modifiers: public
codePointBefore
 Exceptions: NONE
 Parameter: int
 Return Type: int
 Modifiers: public
codePointCount
 Exceptions: NONE
 Parameter: int
 Parameter: int
 Return Type: int
 Modifiers: public
offsetByCodePoints
 Exceptions: NONE
 Parameter: int
 Parameter: int
 Return Type: int
 Modifiers: public
getBytes
 Exception: java.io.UnsupportedEncodingException
 Parameter: java.lang.String
 Return Type: [B
 Modifiers: public
getBytes
 Exceptions: NONE
 Parameter: NONE
 Return Type: [B
 Modifiers: public
getBytes
 Exceptions: NONE
 Parameter: int
 Parameter: int
 Parameter: [B
 Parameter: int
 Return Type: void
 Modifiers: public
getBytes
 Exceptions: NONE
 Parameter: java.nio.charset.Charset
 Return Type: [B
 Modifiers: public
contentEquals
 Exceptions: NONE
 Parameter: java.lang.StringBuffer
 Return Type: boolean
 Modifiers: public
contentEquals
 Exceptions: NONE
 Parameter: java.lang.CharSequence
 Return Type: boolean
 Modifiers: public
regionMatches
 Exceptions: NONE
 Parameter: int
 Parameter: java.lang.String
 Parameter: int
 Parameter: int
 Return Type: boolean
 Modifiers: public
regionMatches
 Exceptions: NONE
 Parameter: boolean
 Parameter: int
 Parameter: java.lang.String
 Parameter: int
 Parameter: int
 Return Type: boolean
 Modifiers: public
startsWith
 Exceptions: NONE
 Parameter: java.lang.String
 Parameter: int
 Return Type: boolean
 Modifiers: public
startsWith
 Exceptions: NONE
 Parameter: java.lang.String
 Return Type: boolean
 Modifiers: public
lastIndexOf
 Exceptions: NONE
 Parameter: java.lang.String
 Return Type: int
 Modifiers: public
lastIndexOf
 Exceptions: NONE
 Parameter: int
 Return Type: int
 Modifiers: public
lastIndexOf
 Exceptions: NONE
 Parameter: int
 Parameter: int
 Return Type: int
 Modifiers: public
lastIndexOf
 Exceptions: NONE
 Parameter: java.lang.String
 Parameter: int
 Return Type: int
 Modifiers: public
substring
 Exceptions: NONE
 Parameter: int
 Parameter: int
 Return Type: java.lang.String
 Modifiers: public
substring
 Exceptions: NONE
 Parameter: int
 Return Type: java.lang.String
 Modifiers: public
isEmpty
 Exceptions: NONE
 Parameter: NONE
 Return Type: boolean
 Modifiers: public
replace
 Exceptions: NONE
 Parameter: char
 Parameter: char
 Return Type: java.lang.String
 Modifiers: public
replace
 Exceptions: NONE
 Parameter: java.lang.CharSequence
 Parameter: java.lang.CharSequence
 Return Type: java.lang.String
 Modifiers: public
matches
 Exceptions: NONE
 Parameter: java.lang.String
 Return Type: boolean
 Modifiers: public
replaceFirst
 Exceptions: NONE
 Parameter: java.lang.String
 Parameter: java.lang.String
 Return Type: java.lang.String
 Modifiers: public
replaceAll
 Exceptions: NONE
 Parameter: java.lang.String
 Parameter: java.lang.String
 Return Type: java.lang.String
 Modifiers: public
split
 Exceptions: NONE
 Parameter: java.lang.String
 Parameter: int
 Return Type: [Ljava.lang.String;
 Modifiers: public
split
 Exceptions: NONE
 Parameter: java.lang.String
 Return Type: [Ljava.lang.String;
 Modifiers: public
toLowerCase
 Exceptions: NONE
 Parameter: NONE
 Return Type: java.lang.String
 Modifiers: public
toLowerCase
 Exceptions: NONE
 Parameter: java.util.Locale
 Return Type: java.lang.String
 Modifiers: public
toUpperCase
 Exceptions: NONE
 Parameter: NONE
 Return Type: java.lang.String
 Modifiers: public
toUpperCase
 Exceptions: NONE
 Parameter: java.util.Locale
 Return Type: java.lang.String
 Modifiers: public
trim
 Exceptions: NONE
 Parameter: NONE
 Return Type: java.lang.String
 Modifiers: public
strip
 Exceptions: NONE
 Parameter: NONE
 Return Type: java.lang.String
 Modifiers: public
stripLeading
 Exceptions: NONE
 Parameter: NONE
 Return Type: java.lang.String
 Modifiers: public
stripTrailing
 Exceptions: NONE
 Parameter: NONE
 Return Type: java.lang.String
 Modifiers: public
lines
 Exceptions: NONE
 Parameter: NONE
 Return Type: java.util.stream.Stream
 Modifiers: public
repeat
 Exceptions: NONE
 Parameter: int
 Return Type: java.lang.String
 Modifiers: public
isBlank
 Exceptions: NONE
 Parameter: NONE
 Return Type: boolean
 Modifiers: public
toCharArray
 Exceptions: NONE
 Parameter: NONE
 Return Type: [C
 Modifiers: public
format
 Exceptions: NONE
 Parameter: java.util.Locale
 Parameter: java.lang.String
 Parameter: [Ljava.lang.Object;
 Return Type: java.lang.String
 Modifiers: public static transient
format
 Exceptions: NONE
 Parameter: java.lang.String
 Parameter: [Ljava.lang.Object;
 Return Type: java.lang.String
 Modifiers: public static transient
resolveConstantDesc
 Exceptions: NONE
 Parameter: java.lang.invoke.MethodHandles$Lookup
 Return Type: java.lang.String
 Modifiers: public
resolveConstantDesc
 Exception: java.lang.ReflectiveOperationException
 Parameter: java.lang.invoke.MethodHandles$Lookup
 Return Type: java.lang.Object
 Modifiers: public volatile
codePoints
 Exceptions: NONE
 Parameter: NONE
 Return Type: java.util.stream.IntStream
 Modifiers: public
equalsIgnoreCase
 Exceptions: NONE
 Parameter: java.lang.String
 Return Type: boolean
 Modifiers: public
compareToIgnoreCase
 Exceptions: NONE
 Parameter: java.lang.String
 Return Type: int
 Modifiers: public
endsWith
 Exceptions: NONE
 Parameter: java.lang.String
 Return Type: boolean
 Modifiers: public
subSequence
 Exceptions: NONE
 Parameter: int
 Parameter: int
 Return Type: java.lang.CharSequence
 Modifiers: public
concat
 Exceptions: NONE
 Parameter: java.lang.String
 Return Type: java.lang.String
 Modifiers: public
contains
 Exceptions: NONE
 Parameter: java.lang.CharSequence
 Return Type: boolean
 Modifiers: public
join
 Exceptions: NONE
 Parameter: java.lang.CharSequence
 Parameter: [Ljava.lang.CharSequence;
 Return Type: java.lang.String
 Modifiers: public static transient
join
 Exceptions: NONE
 Parameter: java.lang.CharSequence
 Parameter: java.lang.Iterable
 Return Type: java.lang.String
 Modifiers: public static
indent
 Exceptions: NONE
 Parameter: int
 Return Type: java.lang.String
 Modifiers: public
stripIndent
 Exceptions: NONE
 Parameter: NONE
 Return Type: java.lang.String
 Modifiers: public
translateEscapes
 Exceptions: NONE
 Parameter: NONE
 Return Type: java.lang.String
 Modifiers: public
chars
 Exceptions: NONE
 Parameter: NONE
 Return Type: java.util.stream.IntStream
 Modifiers: public
transform
 Exceptions: NONE
 Parameter: java.util.function.Function
 Return Type: java.lang.Object
 Modifiers: public
formatted
 Exceptions: NONE
 Parameter: [Ljava.lang.Object;
 Return Type: java.lang.String
 Modifiers: public transient
copyValueOf
 Exceptions: NONE
 Parameter: [C
 Parameter: int
 Parameter: int
 Return Type: java.lang.String
 Modifiers: public static
copyValueOf
 Exceptions: NONE
 Parameter: [C
 Return Type: java.lang.String
 Modifiers: public static
intern
 Exceptions: NONE
 Parameter: NONE
 Return Type: java.lang.String
 Modifiers: public native
describeConstable
 Exceptions: NONE
 Parameter: NONE
 Return Type: java.util.Optional
 Modifiers: public
wait
 Exception: java.lang.InterruptedException
 Parameter: long
 Parameter: int
 Return Type: void
 Modifiers: public final
wait
 Exception: java.lang.InterruptedException
 Parameter: NONE
 Return Type: void
 Modifiers: public final
wait
 Exception: java.lang.InterruptedException
 Parameter: long
 Return Type: void
 Modifiers: public final native
getClass
 Exceptions: NONE
 Parameter: NONE
 Return Type: java.lang.Class
 Modifiers: public final native
notify
 Exceptions: NONE
 Parameter: NONE
 Return Type: void
 Modifiers: public final native
notifyAll
 Exceptions: NONE
 Parameter: NONE
 Return Type: void
 Modifiers: public final native

FIELDS
CASE_INSENSITIVE_ORDER
 Type: java.util.Comparator
 Modifiers: public static final
We never enter the above conditionals :(
======================================================
